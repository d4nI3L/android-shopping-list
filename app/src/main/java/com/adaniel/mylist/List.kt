package com.adaniel.mylist


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewConfiguration
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.shopping_list_layout.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [List.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class List : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listView: ListView? = null
    private var myList: ArrayList<Product> = ArrayList<Product>()
    private var adapter: ShoppingListAdapter? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)

        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mView = inflater.inflate(R.layout.fragment_list, container, false)
        listView = mView.findViewById<ListView>(R.id.listToBuy)

        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        myList = ProductTableHelper(context).getInListProducts(1)
        adapter = ShoppingListAdapter(context, myList)
        listView?.adapter = adapter
        listView?.setOnItemClickListener { parent, view, position, id ->
            val product = myList.get(position)
            if (product.inBag == 1) {
                ProductTableHelper(context).updateDataInBag(product.id, 0)
                product.inBag = 0

            } else {
                ProductTableHelper(context).updateDataInBag(product.id, 1)
                product.inBag = 1

            }
            adapter?.shoppingList = myList
            adapter?.notifyDataSetChanged()

        }
    }


    private fun recognizeClicked(doubleC: Boolean, position: Int, newTime: Long) {
        if (!doubleC) {
            shoppingBag(myList.get(position), newTime)
        } else {
            heart(myList.get(position), newTime)
        }

    }

    private fun shoppingBag(product: Product, newTime: Long) {
        Log.i("Test", "click")
        if (product.inBag == 1) {

            ProductTableHelper(context).updateDataInBag(product.id, 0)
            product.inBag = 0

        } else {

            ProductTableHelper(context).updateDataInBag(product.id, 1)
            product.inBag = 1

        }

        adapter?.shoppingList = myList
        adapter?.notifyDataSetChanged()
    }

    private fun heart(product: Product, newTime: Long) {
        Log.i("Test", "double click")
        if (product.favorite == 1) {

            ProductTableHelper(context).updateDataFavorite(product.id, 0)
            product.favorite = 0


        } else {
            ProductTableHelper(context).updateDataFavorite(product.id, 1)
            product.favorite = 1

        }

        adapter?.shoppingList = myList

        adapter?.notifyDataSetChanged()

    }

    /*
    time1 = System.currentTimeMillis()
            val diff = (time1 - time0).toInt()
            val i =300
            Log.i("Test", "Die Differenz ist: $diff")

            if (diff < i && click != 1) {
                recognizeClicked(true, position, time1)

            } else {
                if(click == 1) {
                    recognizeClicked(false, position, time1)
                    click = 0
                } else {
                    recognizeClicked(false, position, time1)
                    click ++
                }

            }

            Handler().postDelayed({

            }, 100)
            time0 = System.currentTimeMillis()

        }
     if (product.inBag == 1) {

                    ProductTableHelper(context).updateDataInBag(product.id, 0)
                    product.inBag = 0



                } else {

                    ProductTableHelper(context).updateDataInBag(product.id, 1)
                    product.inBag = 1



                }
            } else {
                val product = myList.get(position)
                if (product.favorite == 1) {

                    ProductTableHelper(context).updateDataFavorite(product.id, 0)
                    product.favorite = 0


                } else {
                    ProductTableHelper(context).updateDataFavorite(product.id, 1)
                    product.favorite = 1


                }

            }
     */

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment List.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            List().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
