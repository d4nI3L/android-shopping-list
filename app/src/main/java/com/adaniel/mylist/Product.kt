package com.adaniel.mylist



data class Product(
    var id: Int,
    var amount: Int,
    var name: String,
    var categorie: String,
    var shop: String,
    var inBag: Int,
    var favorite: Int,
    var inlist: Int,
    var note: String
)