package com.adaniel.mylist

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add.*
import java.util.*

class AddActivity : AppCompatActivity() {

    private var editAmount: Int = 1
    private var editName: String? = null
    private var editCategory: String? = null
    private var editShop: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        val actionBar = supportActionBar
        actionBar!!.title = getString(R.string.add_items_title)

        addBtn.setOnClickListener{
            try {
                editAmount = Integer.parseInt(editTextAmount.text.toString())
                //Log.i("Test", "$editAmount")
            } catch(e: Exception) {
                editAmount = 1
            }

            editName =editTextName.text.toString()
            editCategory = "${editTextCategory.text.toString()}"
            editShop ="${editTextShop.text.toString()}"

            if(editName.isNullOrEmpty()) {
                Toast.makeText(this, this.getString(R.string.add_amount_error), Toast.LENGTH_SHORT ).show()
            } else {
                // here i save the product!!!
                saveNow()
                //
                editTextName.text.clear()
                editTextAmount.text.clear()
                editTextCategory.text.clear()
                editTextShop.text.clear()
                Toast.makeText(this, this.getString(R.string.add_to_shoppinglist), Toast.LENGTH_SHORT ).show()
            }


           // Log.i("Test", "$editAmount $editName")
        }

        fabToList.setOnClickListener {
            val addIntent = Intent(this, MainActivity::class.java)
            startActivity(addIntent)
        }



    }

    fun saveNow() {

        val name = editName.toString()
        val amount = editAmount
        val category = editCategory.toString()
        val shop = editShop.toString()


        val data = Product(0, amount, name, category, shop, 0, 0, 1, "")

        val dataID = ProductTableHelper(applicationContext).saveNewEntry(data)

       // Log.i("Test", "DataID. $dataID $amount $name")
        //ProductTableHelper(applicationContext).deleteAllRows()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.top_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
}
