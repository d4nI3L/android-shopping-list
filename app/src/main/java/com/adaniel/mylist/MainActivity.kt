package com.adaniel.mylist

import android.content.ClipData
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Checkable
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var textMessage: TextView
    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.List -> {
                openFragment(List())
                //textMessage.setText(R.string.title_home)
                return@OnNavigationItemSelectedListener true
            }
            R.id.ShoppingBAg -> {
               // textMessage.setText(R.string.title_dashboard)
                openFragment(ShoppingBag())
                return@OnNavigationItemSelectedListener true
            }
            R.id.Favorites -> {
                openFragment(Favorites())
               // Toast.makeText(this, getString(R.string.click_for_details_fragment), Toast.LENGTH_SHORT).show()
               // textMessage.setText(R.string.title_notifications)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        openFragment(List())

        var fragM = " "
        if(intent.extras != null) {
            fragM = intent.extras.getString("frag")
            if(fragM.equals("Fav")) {
                openFragment(Favorites())
                //(R.id.Favorites as Checkable).isChecked = true
                //(R.id.List as Checkable).isChecked = false
            }

        }



        //textMessage = findViewById(R.id.message)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        //my default page is the listview of the items the user wants to buy


        addItemBtn.setOnClickListener {
            val addIntent = Intent(this, AddActivity::class.java)
            startActivity(addIntent)
        }

        // Product
       // val data = Product(1, 2, "Gurken", "Essen", "Lidl", 0, 0)
       // val test = ProductTableHelper(applicationContext).getProduct(2)
        //Log.i("Test", "${test.id}, ${test.name}")
        //val test1 = ProductTableHelper(applicationContext).getProduct(4)
        //Log.i("Test", "${test1.id}, ${test1.name}")
    }

    /**
     * @param fragment This method allows you to open any fragment without
     * causing any problems
     *
     */
    public fun openFragment(fragment: Fragment) {
        try {
            val manager = supportFragmentManager
            val transAction = manager.beginTransaction()
            transAction.replace(R.id.include, fragment)
            transAction.addToBackStack(null)
            transAction.commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.top_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.deleteList -> {
                Toast.makeText(this, getString(R.string.deleted_list), Toast.LENGTH_SHORT).show()
                ProductTableHelper(applicationContext).deleteRestOnList()
                recreate()
                return true
            }

            R.id.delteShoppingBAg -> {
                Toast.makeText(this, getString(R.string.deleted_bag), Toast.LENGTH_SHORT).show()
                ProductTableHelper(applicationContext).deleteAllInBag()
                recreate()
                return true
            }

            R.id.deleteFavorites -> {
                Toast.makeText(this, getString(R.string.deleted_fav), Toast.LENGTH_SHORT).show()
                ProductTableHelper(applicationContext).deleteAllFav()
                recreate()
                return true
            }
            R.id.deleteAll -> {
                Toast.makeText(this, getString(R.string.deleted_all), Toast.LENGTH_SHORT).show()
                ProductTableHelper(applicationContext).deleteAllRows()
                recreate()

                return true
            }


        }

        return super.onOptionsItemSelected(item)
    }
}
