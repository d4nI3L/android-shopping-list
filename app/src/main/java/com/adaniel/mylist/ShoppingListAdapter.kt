package com.adaniel.mylist


import android.content.Context
import android.database.DataSetObserver
import android.graphics.Paint


import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class ShoppingListAdapter(context: Context?, shoppingList: ArrayList<Product>): BaseAdapter() {

    private val context: Context?
    var shoppingList: ArrayList<Product>

    init {
        this.context = context
        this.shoppingList = shoppingList

    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val layoutInf = LayoutInflater.from(context)
        val listInflate = layoutInf.inflate(R.layout.shopping_list_layout, parent, false)
        val name = listInflate.findViewById<TextView>(R.id.viewProductName)
        val image = listInflate.findViewById<ImageView>(R.id.imageBought)
        val fav = listInflate.findViewById<ImageView>(R.id.listFav)
        val category = listInflate.findViewById<TextView>(R.id.textView10)
        val cat2 =  listInflate.findViewById<TextView>(R.id.textView9)
        val shop2 =  listInflate.findViewById<TextView>(R.id.textView11)
        val shop = listInflate.findViewById<TextView>(R.id.textView12)

        if(!(shoppingList.get(position).categorie.isBlank())) {
           category.visibility = VISIBLE
            cat2.visibility = VISIBLE
        } else {
            category.visibility = GONE
            cat2.visibility = GONE
        }
        if(!(shoppingList.get(position).shop.isBlank())) {
            shop.visibility = VISIBLE
            shop2.visibility = VISIBLE
        } else {
            shop.visibility = GONE
            shop2.visibility = GONE
        }

        if(shoppingList.get(position).inBag == 1) {
            image.visibility = View.VISIBLE
            name.paintFlags= name.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG


        } else {
            name.paintFlags= name.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG
            image.visibility = View.INVISIBLE
        }
        if(shoppingList.get(position).favorite == 1) {
           fav.visibility = View.VISIBLE
        } else {
           fav.visibility = View.INVISIBLE
        }

        category.text = shoppingList.get(position).categorie
        shop.text = shoppingList.get(position).shop
        name.text = "${shoppingList[position].amount} x ${shoppingList[position].name}"
        //this.time = System.currentTimeMillis()
        return listInflate
    }

    override fun getItem(position: Int): Any {
        return shoppingList.get(position)
}
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
       return shoppingList.size
    }





}