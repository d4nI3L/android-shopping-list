package com.adaniel.mylist


import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableStringBuilder

import android.view.Menu
import android.view.MenuItem
import android.view.View.*
import android.widget.Checkable
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_detail_fav.*


class DetailActivityFav : AppCompatActivity() {


    private var id = -1
    private var amount = 0
    private var inList = 0
    private var name =""
    private var category = ""
    private var shop = ""
    private var note = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_fav)

        val actionBar = supportActionBar
        actionBar!!.title = "Your Favourite's Details"

        val bundle = intent.extras

        id = bundle.getInt("id")
        amount = bundle.getInt("amount")
        inList = bundle.getInt("inList")
        editTextAmount.text = SpannableStringBuilder(bundle.getInt("amount").toString())
        editTextNameF.text = SpannableStringBuilder(bundle.getString("name"))
        editTextCategoryF.text = SpannableStringBuilder(bundle.getString("category"))
        editTextShopF.text = SpannableStringBuilder(bundle.getString("shop"))
        editTextDescription.text = SpannableStringBuilder(bundle.getString("note").toString())



        fabFavourites.setOnClickListener {
            name = editTextNameF.text.toString()
            amount =editTextAmount.text.toString().toInt()
            category = editTextCategoryF.text.toString()
            shop =   editTextShopF.text.toString()
            note =  editTextDescription.text.toString()
            ProductTableHelper(applicationContext).updateFavouriteValues(id, name, amount, category, shop, note, inList)
            //(R.id.Favorites as Checkable).isChecked = true
            openFavourites()
        }

        floatingActionButton2.setOnClickListener {
            val v = findViewById<EditText>(R.id.editTextAmount)
            val t = findViewById<TextView>(R.id.amountDetail)
            v.visibility = VISIBLE
            t.visibility = VISIBLE
            inList = 1
            amount = 1

        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.top_fav_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.delete_Fav -> {
                if (id != -1) {
                    Toast.makeText(this, getString(R.string.removed_fav), Toast.LENGTH_SHORT).show()
                    ProductTableHelper(applicationContext).deleteOneProduct(id)
                    openFavourites()
                    return true
                }

            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun openFavourites() {
        val mIntent = Intent(this, MainActivity::class.java)
        val bundle = Bundle()
        bundle.putString("frag", "Fav")
        mIntent.putExtras(bundle)
        startActivity(mIntent)

    }

}
