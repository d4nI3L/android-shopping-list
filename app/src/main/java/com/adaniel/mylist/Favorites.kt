package com.adaniel.mylist


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ListView


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Favorites.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class Favorites : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listView: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mView = inflater.inflate(R.layout.fragment_favorites, container, false)
        listView = mView.findViewById<ListView>(R.id.myFavouriteList)

        return mView
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val myList = ProductTableHelper(context).getFavoriteProducts(1)

        listView?.adapter = FavouriteListAdapter(context, myList)

        listView?.setOnItemClickListener { parent, view, position, id ->

            activity?.let{
                val intent = Intent (it, DetailActivityFav::class.java)
                val bundle = Bundle()
                bundle.putInt("id", myList.get(position).id )
                bundle.putInt("amount", myList.get(position).amount)
                bundle.putString("name", "${myList.get(position).name}")
                bundle.putString("category", "${myList.get(position).categorie}")
                bundle.putString("shop", "${myList.get(position).shop}")
                bundle.putString("description", "${myList.get(position).note}")
                bundle.putInt("inList", myList.get(position).inlist)
                bundle.putString("note", myList.get(position).note)

                intent.putExtras(bundle)
                it.startActivity(intent)
            }
        }



    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Favorites.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Favorites().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
