package com.adaniel.mylist

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.util.Log


class DataBaseHelper(context: Context?): SQLiteOpenHelper(context, DATABASE_NAME, null, DATABSE_VERSION) {

    private val CREATE_PRODUCTS_TABLE = "CREATE TABLE IF NOT EXISTS $TABLE_NAME  (  $_ID_ INTEGER PRIMARY KEY, $AMOUNT INTEGER, $NAME TEXT,$CATEGORIE TEXT, $SHOP TEXT, $INBAG INTEGER, $FAVORITE INTEGER, $INLIST INTEGER, $NOTE TEXT)"

    override fun onCreate(db: SQLiteDatabase?) {
        //db?.execSQL("DROP TABLE $TABLE_NAME")
        db?.execSQL(CREATE_PRODUCTS_TABLE)
        Log.i("test", "tabelle angelegt")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
       db?.execSQL("DROP TABLE $TABLE_NAME")
        db?.execSQL(CREATE_PRODUCTS_TABLE)
        Log.i("test", "tabelle angelegt neu")
    }

    companion object {
        val DATABASE_NAME = "products"
        val TABLE_NAME = "tableP"
        val DATABSE_VERSION = 1
        val _ID_ = "_id_"
        val AMOUNT = "amount"
        val NAME = "NAME"
        val CATEGORIE = "categorie"
        val SHOP = "shop"
        val FAVORITE = "favorite"
        val INBAG = "inbag"
        val INLIST ="inlist"
        val NOTE = "note"

    }


}