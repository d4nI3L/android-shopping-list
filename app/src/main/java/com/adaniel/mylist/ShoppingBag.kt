package com.adaniel.mylist


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ListView


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ShoppingBag.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ShoppingBag : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listView: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mView = inflater.inflate(R.layout.fragment_shopping_bag, container, false)
        listView = mView.findViewById<ListView>(R.id.bagList)

        return mView

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val myList = ProductTableHelper(context).getInBagProducts(1)// get all that are in the bag
        val adapter = ShoppingBagAdapter(context, myList)
        listView?.adapter = adapter


        listView?.setOnItemClickListener { parent, view, position, id ->
            val product = myList.get(position)
            if(product.favorite == 1) {
                ProductTableHelper(context).updateDataFavorite(product.id, 0)
                product.favorite = 0
                adapter.notifyDataSetInvalidated()


            } else {
                ProductTableHelper(context).updateDataFavorite(product.id, 1)
                product.favorite= 1
                adapter.notifyDataSetInvalidated()

            }


        }


    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ShoppingBag.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ShoppingBag().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
