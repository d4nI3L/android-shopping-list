package com.adaniel.mylist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class FavouriteListAdapter(context: Context?, favourites: ArrayList<Product>): BaseAdapter()  {
    private val context: Context?
    private val favourites: ArrayList<Product>

    init {
        this.context = context
        this.favourites = favourites
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInf = LayoutInflater.from(context)
        val listInflate = layoutInf.inflate(R.layout.favourite_list_layout, parent, false)
        val name = listInflate.findViewById<TextView>(R.id.favouriteProduct)
        val category = listInflate.findViewById<TextView>(R.id.favouriteCategory)
        val shop = listInflate.findViewById<TextView>(R.id.favouriteShop)
        val cat2 = listInflate.findViewById<TextView>(R.id.textView4)
        val shop2 = listInflate.findViewById<TextView>(R.id.textView5)
        val description = listInflate.findViewById<TextView>(R.id.favouriteDescription)
        val note  = listInflate.findViewById<TextView>(R.id.textView8)


        if(!(favourites.get(position).categorie.isBlank())) {
            cat2.visibility = View.VISIBLE
            category.visibility = View.VISIBLE

        } else {
            cat2.visibility = View.GONE
            category.visibility = View.GONE

        }
        if(!(favourites.get(position).note.isBlank())) {
            note.visibility = View.VISIBLE
            description.visibility = View.VISIBLE


        } else {
            note.visibility = View.GONE
            description.visibility = View.GONE


        }
        if(!(favourites.get(position).shop.isBlank())) {
            shop2.visibility = View.VISIBLE
            shop.visibility = View.VISIBLE

        } else {
            shop2.visibility = View.GONE
            shop.visibility = View.GONE

        }


        category.text =  favourites[position].categorie
        shop.text =  favourites[position].shop
        name.text = favourites[position].name
        description.text= favourites[position].note

        return listInflate
    }

    override fun getItem(position: Int): Any {
        return favourites.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return favourites.size
    }

}