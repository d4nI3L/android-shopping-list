package com.adaniel.mylist

import android.content.Context
import android.content.ContentValues
import android.util.Log
import java.sql.SQLException

/**
 * This class is basically my interface for the databbase. It handles all events
 * like saving or deleting
 *
 * @param context The Context
 */
class ProductTableHelper(context: Context?) {
    //the actual database
    private val helper = DataBaseHelper(context)

    /**
     * @param data the product that needs to be added to the table
     */
    fun saveNewEntry(data: Product): Any {
        val db = helper.writableDatabase
        val dataset = ContentValues()
        dataset.put(DataBaseHelper.NAME, data.name)
        dataset.put(DataBaseHelper.AMOUNT, data.amount)
        dataset.put(DataBaseHelper.CATEGORIE, data.categorie)
        dataset.put(DataBaseHelper.SHOP, data.shop)
        dataset.put(DataBaseHelper.FAVORITE, data.favorite)
        dataset.put(DataBaseHelper.INBAG, data.inBag)
        dataset.put(DataBaseHelper.INLIST, data.inlist)
        dataset.put(DataBaseHelper.NOTE, data.note)

        db.beginTransaction()
        val id = try {
            val id = db.insert(DataBaseHelper.TABLE_NAME, null, dataset)
            db.setTransactionSuccessful()
            id
        } catch (e: SQLException) {

        } finally {
            db.endTransaction()
            db.close()
        }
        //Log.i("DATA", "DATEN eingefügt")
        return id

    }

    fun getAllProducts(): ArrayList<Product> {
        val mCollection = arrayOf(DataBaseHelper._ID_, DataBaseHelper.AMOUNT,DataBaseHelper.NAME, DataBaseHelper.CATEGORIE,
            DataBaseHelper.SHOP,DataBaseHelper.INBAG,DataBaseHelper.FAVORITE, DataBaseHelper.INLIST, DataBaseHelper.NOTE)

        val db = helper.readableDatabase
        val cursor = db.query(DataBaseHelper.TABLE_NAME, mCollection, null, null, null, null, null)

        val list = ArrayList<Product>() // my product list
        while(cursor.moveToNext()) { // iterate over rows and get entries
            val product = Product(0,  0 , "", "", "", 0, 0, 0, "")
            product.id = cursor.getInt(cursor.getColumnIndex(DataBaseHelper._ID_))
            product.amount = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.AMOUNT))
            product.name = cursor.getString(cursor.getColumnIndex(DataBaseHelper.NAME))
            product.categorie= cursor.getString(cursor.getColumnIndex(DataBaseHelper.CATEGORIE))
            product.shop = cursor.getString(cursor.getColumnIndex(DataBaseHelper.SHOP))
            product.inBag = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.INBAG))
            product.favorite = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.FAVORITE))
            product.inlist = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.INLIST))
            product.note = cursor.getString(cursor.getColumnIndex(DataBaseHelper.NOTE))
            list.add(product)

            Log.i("Test", "Die liste enthält ${list.size} Elemente")

        }


        cursor.close()
        db.close()
        return list
    }



    fun getProduct(id: Int): Product {
        val db = helper.readableDatabase
        val query = "SELECT ${DataBaseHelper._ID_}, ${DataBaseHelper.AMOUNT}, ${DataBaseHelper.NAME}, ${DataBaseHelper.CATEGORIE}, ${DataBaseHelper.SHOP}, ${DataBaseHelper.INBAG}, ${DataBaseHelper.FAVORITE}, ${DataBaseHelper.INLIST}, ${DataBaseHelper.NOTE} " +
                "FROM ${DataBaseHelper.TABLE_NAME} WHERE _id_ = $id;"
        val cursor = db.rawQuery(query, null, null)
        val product = Product(0, 0, "", "", "", 0, 0, 0, "")
        while(cursor.moveToNext()) { // iterate over rows and get entries
            product.id = id
            product.amount = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.AMOUNT))
            product.name = cursor.getString(cursor.getColumnIndex(DataBaseHelper.NAME))
            product.categorie = cursor.getString(cursor.getColumnIndex(DataBaseHelper.CATEGORIE))
            product.shop = cursor.getString(cursor.getColumnIndex(DataBaseHelper.SHOP))
            product.inBag = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.INBAG))
            product.favorite = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.FAVORITE))
            product.inlist = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.INLIST))
            product.note = cursor.getString(cursor.getColumnIndex(DataBaseHelper.NOTE))
            //Log.i("Test", "$id: ${product.name}")
        }
        cursor.close()
        db.close()
        return product
    }

    fun getInListProducts(inList: Int): ArrayList<Product> {
        val db = helper.readableDatabase
        val query = "SELECT ${DataBaseHelper._ID_}, ${DataBaseHelper.AMOUNT}, ${DataBaseHelper.NAME}, ${DataBaseHelper.CATEGORIE}, ${DataBaseHelper.SHOP}, ${DataBaseHelper.INBAG}, ${DataBaseHelper.FAVORITE}, ${DataBaseHelper.INLIST}, ${DataBaseHelper.NOTE} " +
                "FROM ${DataBaseHelper.TABLE_NAME} WHERE inlist = $inList;"
        val cursor = db.rawQuery(query, null, null)
        val list = ArrayList<Product>()

        while(cursor.moveToNext()) { // iterate over rows and get entries
            val product = Product(0, 0, "", "", "", 0, 0, 0, "")
            product.id =  cursor.getInt(cursor.getColumnIndex(DataBaseHelper._ID_))
            product.amount = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.AMOUNT))
            product.name = cursor.getString(cursor.getColumnIndex(DataBaseHelper.NAME))
            product.categorie = cursor.getString(cursor.getColumnIndex(DataBaseHelper.CATEGORIE))
            product.shop = cursor.getString(cursor.getColumnIndex(DataBaseHelper.SHOP))
            product.inBag = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.INBAG))
            product.favorite = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.FAVORITE))
            product.inlist = inList
            product.note = cursor.getString(cursor.getColumnIndex(DataBaseHelper.NOTE))
           // Log.i("Test", "Bag: $inbag: ${product.name}")
            list.add(product)
        }
        cursor.close()
        db.close()
        return list
    }

    fun getInBagProducts(inbag: Int): ArrayList<Product> {
        val db = helper.readableDatabase
        val query = "SELECT ${DataBaseHelper._ID_}, ${DataBaseHelper.AMOUNT}, ${DataBaseHelper.NAME}, ${DataBaseHelper.CATEGORIE}, ${DataBaseHelper.SHOP}, ${DataBaseHelper.INBAG}, ${DataBaseHelper.FAVORITE}, ${DataBaseHelper.INLIST}, ${DataBaseHelper.NOTE} " +
                "FROM ${DataBaseHelper.TABLE_NAME} WHERE inbag= $inbag;"
        val cursor = db.rawQuery(query, null, null)
        val list = ArrayList<Product>()

        while(cursor.moveToNext()) { // iterate over rows and get entries
            val product = Product(0, 0, "", "", "", 0, 0, 0, "")
            product.id =  cursor.getInt(cursor.getColumnIndex(DataBaseHelper._ID_))
            product.amount = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.AMOUNT))
            product.name = cursor.getString(cursor.getColumnIndex(DataBaseHelper.NAME))
            product.categorie = cursor.getString(cursor.getColumnIndex(DataBaseHelper.CATEGORIE))
            product.shop = cursor.getString(cursor.getColumnIndex(DataBaseHelper.SHOP))
            product.inBag = inbag
            product.favorite = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.FAVORITE))
            product.inlist = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.INLIST))
            product.note = cursor.getString(cursor.getColumnIndex(DataBaseHelper.NOTE))
            Log.i("Test", "Bag: $inbag: ${product.name}")
            list.add(product)
        }
        cursor.close()
        db.close()
        return list
    }

    fun getFavoriteProducts(fav: Int): ArrayList<Product> {
        val db = helper.readableDatabase
        val query = "SELECT ${DataBaseHelper._ID_}, ${DataBaseHelper.AMOUNT}, ${DataBaseHelper.NAME}, ${DataBaseHelper.CATEGORIE}, ${DataBaseHelper.SHOP}, ${DataBaseHelper.INBAG}, ${DataBaseHelper.FAVORITE} , ${DataBaseHelper.INLIST}, ${DataBaseHelper.NOTE} " +
                "FROM ${DataBaseHelper.TABLE_NAME} WHERE favorite= $fav;"
        val cursor = db.rawQuery(query, null, null)

        val list = ArrayList<Product>()
        while(cursor.moveToNext()) { // iterate over rows and get entries
            val product = Product(0, 0, "", "", "", 0, 0, 0, "")
            product.id =  cursor.getInt(cursor.getColumnIndex(DataBaseHelper._ID_))
            product.amount = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.AMOUNT))
            product.name = cursor.getString(cursor.getColumnIndex(DataBaseHelper.NAME))
            product.categorie = cursor.getString(cursor.getColumnIndex(DataBaseHelper.CATEGORIE))
            product.shop = cursor.getString(cursor.getColumnIndex(DataBaseHelper.SHOP))
            product.inBag =  cursor.getInt(cursor.getColumnIndex(DataBaseHelper.INBAG))
            product.favorite = fav
            product.inlist = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.INLIST))
            product.note = cursor.getString(cursor.getColumnIndex(DataBaseHelper.NOTE))
            //Log.i("Test", "$id: ${product.name}")
            list.add(product)
        }
        cursor.close()
        db.close()
        return list
    }
    fun updateFavouriteValues(id: Int, name: String, amount: Int, category: String, shop: String, note: String, inList: Int) {
        val db = helper.writableDatabase
        val  values = ContentValues()
        values.put(DataBaseHelper.NAME, name)
        values.put(DataBaseHelper.AMOUNT, amount)
        values.put(DataBaseHelper.CATEGORIE, category)
        values.put(DataBaseHelper.SHOP, shop)
        values.put(DataBaseHelper.NOTE, note)
        values.put(DataBaseHelper.INLIST, inList)
        db.update(DataBaseHelper.TABLE_NAME, values, "${DataBaseHelper._ID_}= $id", null)
        db.close()
        //Log.i("Test", "Updated to $inbag")

    }

    fun updateDataInBag(id: Int, inbag: Int) {
        val db = helper.writableDatabase
        val  values = ContentValues()
        values.put(DataBaseHelper.INBAG, inbag)
        db.update(DataBaseHelper.TABLE_NAME, values, "${DataBaseHelper._ID_}= $id", null)
        db.close()
       // Log.i("Test", "Updated to $inbag")

    }


    fun updateDataFavorite(id: Int, fav: Int) {
        val db = helper.writableDatabase
        val  values = ContentValues()
        values.put(DataBaseHelper.FAVORITE, fav)
        db.update(DataBaseHelper.TABLE_NAME, values, "${DataBaseHelper._ID_}= $id", null)
        db.close()

    }

    fun deleteOneProduct(id: Int) {
        val db = helper.writableDatabase
        db.delete(DataBaseHelper.TABLE_NAME, "${DataBaseHelper._ID_}= $id", null)
        db.close()
    }

    fun deleteAllRows() {
        val db = helper.writableDatabase
        db.delete(DataBaseHelper.TABLE_NAME, null, null)
        db.close()
    }

    fun deleteAllInBag() {
        updateInBag0Fav()
        val db = helper.writableDatabase
        db.delete(DataBaseHelper.TABLE_NAME, "${DataBaseHelper.INBAG}= 1", null)
        db.close()
    }

    fun deleteAllFav() {
        val db = helper.writableDatabase
        db.delete(DataBaseHelper.TABLE_NAME, "${DataBaseHelper.FAVORITE}= 1", null)
        db.close()
    }

    fun deleteRestOnList() {
        updateInList0()
        val db = helper.writableDatabase
        db.delete(DataBaseHelper.TABLE_NAME, "${DataBaseHelper.INLIST} = 1", null)
        db.close()
    }

    /**
     * Sets all Favourites in Shopping bag to inbag = 0, so they wont be affected by delete
     * method.
     */
    fun updateInBag0Fav(){
        val db = helper.writableDatabase
        val  values = ContentValues()
        values.put(DataBaseHelper.INBAG, 0)
        db.update(DataBaseHelper.TABLE_NAME, values, "${DataBaseHelper.FAVORITE}= 1", null)
        db.close()
        //Log.i("Test", "Updated to $inbag")
    }

    /**
     * Sets all favourites and shopping bag items to inLIst = 0.
     */
    fun  updateInList0(){
        val db = helper.writableDatabase
        val  values = ContentValues()
        values.put(DataBaseHelper.INLIST, 0)
        db.update(DataBaseHelper.TABLE_NAME, values, "${DataBaseHelper.INBAG}= 1 OR ${DataBaseHelper.FAVORITE} = 1", null)
        db.close()
    }





}