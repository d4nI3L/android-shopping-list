package com.adaniel.mylist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class ShoppingBagAdapter (context: Context?, shoppingBag: ArrayList<Product>): BaseAdapter() {

    private val context: Context?
    private val shoppingBag: ArrayList<Product>

    init {
        this.context = context
        this.shoppingBag = shoppingBag
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInf = LayoutInflater.from(context)
        val listInflate = layoutInf.inflate(R.layout.shopping_bag_layout, parent, false)
        val name = listInflate.findViewById<TextView>(R.id.viewProductNameB)
        val amount = listInflate.findViewById<TextView>(R.id.viewAmountB)
        val image = listInflate.findViewById<ImageView>(R.id.bagFav)
        val shop = listInflate.findViewById<TextView>(R.id.textViewShopBag)
        val category = listInflate.findViewById<TextView>(R.id.textViewCatBag)
        val cat2 = listInflate.findViewById<TextView>(R.id.textViewBagvisC)
        val shop2 = listInflate.findViewById<TextView>(R.id.textViewBagVisS)

        if(!(shoppingBag.get(position).categorie.isBlank())) {
            cat2.visibility = View.VISIBLE
            category.visibility = View.VISIBLE
        }else {
            cat2.visibility = View.GONE
            category.visibility = View.GONE

        }
        if(!(shoppingBag.get(position).shop.isBlank())) {
            shop2.visibility = View.VISIBLE
            shop.visibility = View.VISIBLE

        } else {
            shop2.visibility = View.GONE
            shop.visibility = View.GONE

        }
        if(shoppingBag.get(position).favorite == 1) {
           image.visibility = View.VISIBLE

        } else {
            image.visibility = View.INVISIBLE
        }
        category.text = shoppingBag[position].categorie
        shop.text = shoppingBag[position].shop
        name.text = shoppingBag[position].name
        amount.text ="${shoppingBag[position].amount} x "

        return listInflate
    }

    override fun getItem(position: Int): Any {
        return shoppingBag.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return shoppingBag.size
    }

}